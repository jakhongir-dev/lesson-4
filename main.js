"use strict";

// Assignment 4
// Task 1
function charReverseHandler(str) {
  const reversed = str
    .split(" ")
    .map((el) =>
      el
        .split("")
        .map((el) => {
          if (el.charCodeAt() >= 97 && el.charCodeAt() <= 122) {
            return el.toUpperCase();
          } else {
            return el.toLowerCase();
          }
        })
        .join("")
    )
    .join(" ");
  console.log(reversed);
}
charReverseHandler("tHe BrOwN fOx iS ChILLinG ouT");

// Task 2
const input = document.querySelector(".input");
const addBtn = document.querySelector(".btn");

const todoContainer = document.querySelector(".todo-container");
const todo = document.querySelector(".todo");
const done = document.querySelector(".done");
const text = document.querySelector(".todo__text");
const dltBtn = document.querySelector(".dlt-icon");

let todosArray = [];
todo.remove();

todoContainer.addEventListener("click", (e) => {
  if (e.target.className == "dlt-icon") {
    todosArray = todosArray.filter(
      (el, index) => `r${index}` != `${e.target.parentElement.id}`
    );
    console.log(todosArray);
    renderList();
  }
});

addBtn.addEventListener("click", (e) => {
  e.preventDefault();
  todosArray.push(input.value);
  renderList();
  input.value = "";
});

function renderList() {
  todoContainer.innerHTML = todosArray.map((el, index) => {
    return `
    <div class='todo' id='r${index}' >
      <input type='checkbox' class='done' />
      <p class='todo__text'>${el}</p>
      <img class='dlt-icon' src='delete.png' />
    </div>`;
  });
}

// Task 3
const array = [1, 2, 88, 1, 3, 4, 88, 3, 5];
const array2 = ["wow", "lol", "wow", "ok", "nah", "ok"];

const findDublicatedHandler = (arr) =>
  arr.filter((item, index) => arr.indexOf(item) !== index);

console.log(findDublicatedHandler(array));
console.log(findDublicatedHandler(array2));

// Task 4
const unionHandler = (arr, arr2) => Array.from(new Set(arr.concat(arr2)));
console.log(unionHandler([1, 2, 3, 4, 5], [1, "a", "f", 3, 4, 5]));

// Task 5
const remover = function (arr) {
  const types = [null, NaN, 0, '"', undefined, false];
  const filtered = arr.filter((el) => !types.includes(el));
  console.log(filtered);
};
remover([null, "hello", false, NaN, 0, undefined, "Wow", "yoy"]);

// Task 6
function sortAlphabetically(str) {
  const strr = str
    .split("")
    .sort((a, b) => a.localeCompare(b))
    .join("");
  console.log(strr);
}

sortAlphabetically("ABrownFoxJumpedOverTheLazyDogsquickly"); // lol
